import {
  ActeLegislatif,
  CodeTypeOrgane,
  existingDateToJson,
  localIso8601StringFromDate,
  patchedDateToJson,
  TypeMandat,
} from "@tricoteuses/assemblee"
import { EnabledDatasets, loadAssembleeData } from "@tricoteuses/assemblee/lib/loaders"
import assert from "assert"
import commandLineArgs from "command-line-args"

import { db } from "../database"

const optionsDefinitions = [
  {
    alias: "l",
    defaultValue: "15",
    name: "legislature",
    type: String,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)
const enabledDatasets =
  EnabledDatasets.ActeursEtOrganes |
  EnabledDatasets.Agendas |
  EnabledDatasets.Amendements |
  EnabledDatasets.DossiersLegislatifs |
  EnabledDatasets.Photos |
  EnabledDatasets.Scrutins
const {
  acteurByUid,
  amendementByUid,
  documentByUid,
  dossierParlementaireByUid,
  organeByUid,
  photoByUid,
  reunionByUid,
  scrutinByUid,
} = loadAssembleeData(
  options.dataDir,
  enabledDatasets,
  options.legislature,
  // {
  //   log: true,
  // },
)

function datesProchesFromActes(
  actes: ActeLegislatif[],
  now: Date,
  dateDernierActe?: Date,
  dateProchainActe?: Date,
) {
  for (const acte of actes) {
    const dateActe = acte.dateActe
    if (dateActe !== undefined) {
      if (dateActe < now) {
        if (dateDernierActe === undefined || dateDernierActe < dateActe) {
          dateDernierActe = dateActe
        }
      } else {
        if (dateProchainActe === undefined || dateProchainActe > dateActe) {
          dateProchainActe = dateActe
        }
      }
    }
    if (acte.actesLegislatifs !== undefined) {
      ;[dateDernierActe, dateProchainActe] = datesProchesFromActes(
        acte.actesLegislatifs,
        now,
        dateDernierActe,
        dateProchainActe,
      )
    }
  }
  return [dateDernierActe, dateProchainActe]
}

async function main() {
  const dataDir = options.dataDir
  assert(dataDir, "Missing argument: data directory")

  // When converting dates to JSON, keep the timezone.
  Date.prototype.toJSON = patchedDateToJson

  const now = new Date()

  if (enabledDatasets & EnabledDatasets.ActeursEtOrganes) {
    {
      if (!options.silent) {
        console.log('Updating "acteurs"…')
      }

      const existingActeursUids = new Set(
        (await db.any(
          `
          SELECT uid
          FROM acteurs
        `,
        )).map(({ uid }) => uid),
      )

      for (const acteur of Object.values(acteurByUid)) {
        let data = acteur
        if (enabledDatasets & EnabledDatasets.Photos) {
          const photo = photoByUid[acteur.uid]
          if (photo !== undefined) {
            data = {
              ...data,
              photo,
            }
          }
        }

        const deputeActif =
          (acteur.mandats || []).filter(
            mandat =>
              mandat.xsiType === TypeMandat.MandatParlementaireType &&
              mandat.legislature === options.legislature &&
              mandat.typeOrgane === CodeTypeOrgane.Assemblee &&
              mandat.dateDebut <= now &&
              (!mandat.dateFin || mandat.dateFin >= now),
          ).length > 0

        await db.none(
          `
            INSERT INTO acteurs (
              uid,
              data,
              depute_actif
            )
            VALUES (
              $<uid>,
              $<data:json>,
              $<deputeActif>
            )
            ON CONFLICT (uid)
            DO UPDATE SET
              data = $<data:json>,
              depute_actif = $<deputeActif>
          `,
          {
            data,
            deputeActif,
            uid: acteur.uid,
          },
        )
        existingActeursUids.delete(acteur.uid)
      }

      // Delete obsolete acteurs.
      for (const obsoleteActeurUid of existingActeursUids) {
        await db.none(
          `
            DELETE FROM acteurs
            WHERE
              uid = $<obsoleteActeurUid>
          `,
          {
            obsoleteActeurUid,
          },
        )
      }
    }

    {
      if (!options.silent) {
        console.log('Updating "organes"…')
      }

      const existingOrganesUids = new Set(
        (await db.any(
          `
          SELECT uid
          FROM organes
        `,
        )).map(({ uid }) => uid),
      )

      for (const organe of Object.values(organeByUid)) {
        await db.none(
          `
            INSERT INTO organes (
              uid,
              data
            )
            VALUES (
              $<uid>,
              $<data:json>
            )
            ON CONFLICT (uid)
            DO UPDATE SET
              data = $<data:json>
          `,
          {
            data: organe,
            uid: organe.uid,
          },
        )
        existingOrganesUids.delete(organe.uid)
      }

      // Delete obsolete organes.
      for (const obsoleteOrganeUid of existingOrganesUids) {
        await db.none(
          `
            DELETE FROM organes
            WHERE
              uid = $<obsoleteOrganeUid>
          `,
          {
            obsoleteOrganeUid,
          },
        )
      }
    }
  }

  if (enabledDatasets & EnabledDatasets.Agendas) {
    {
      if (!options.silent) {
        console.log('Updating "reunions"…')
      }

      const existingReunionsUids = new Set(
        (await db.any(
          `
          SELECT uid
          FROM reunions
        `,
        )).map(({ uid }) => uid),
      )
      const existingActeursDemandeursKeys = new Set(
        (await db.any(
          `
          SELECT *
          FROM reunions_acteurs_demandeurs
        `,
        )).map(({ acteur_uid, reunion_uid }) => `${reunion_uid}_${acteur_uid}`),
      )
      const existingActeursParticipantsKeys = new Set(
        (await db.any(
          `
          SELECT *
          FROM reunions_acteurs_participants
        `,
        )).map(({ acteur_uid, reunion_uid }) => `${reunion_uid}_${acteur_uid}`),
      )
      const existingOrganesDemandeursKeys = new Set(
        (await db.any(
          `
          SELECT *
          FROM reunions_organes_demandeurs
        `,
        )).map(({ organe_uid, reunion_uid }) => `${reunion_uid}_${organe_uid}`),
      )
      const existingOrganesReunisKeys = new Set(
        (await db.any(
          `
          SELECT *
          FROM reunions_organes_reunis
        `,
        )).map(({ organe_uid, reunion_uid }) => `${reunion_uid}_${organe_uid}`),
      )

      for (const reunion of Object.values(reunionByUid)) {
        const jourDebut = localIso8601StringFromDate(reunion.timestampDebut)
        const jourFin =
          reunion.timestampFin === undefined
            ? jourDebut
            : localIso8601StringFromDate(reunion.timestampFin)
        await db.none(
          `
            INSERT INTO reunions (
              uid,
              data,
              jour_debut,
              jour_fin,
              lieu_code,
              xsi_type
            )
            VALUES (
              $<uid>,
              $<data:json>,
              $<jourDebut>,
              $<jourFin>,
              $<lieuCode>,
              $<xsiType>
            )
            ON CONFLICT (uid)
            DO UPDATE SET
              data = $<data:json>,
              jour_debut = $<jourDebut:json>,
              jour_fin = $<jourFin:json>,
              lieu_code = $<lieuCode>,
              xsi_type = $<xsiType:json>
          `,
          {
            data: reunion,
            jourDebut,
            jourFin,
            lieuCode: reunion.lieu === undefined ? null : reunion.lieu.code || null,
            uid: reunion.uid,
            xsiType: reunion.xsiType,
          },
        )
        existingReunionsUids.delete(reunion.uid)

        const demandeurs = reunion.demandeurs
        if (demandeurs !== undefined) {
          for (const { acteurRef } of demandeurs.acteurs || []) {
            if (!existingActeursDemandeursKeys.delete(`${reunion.uid}_${acteurRef}`)) {
              await db.none(
                `
                  INSERT INTO reunions_acteurs_demandeurs (
                    reunion_uid,
                    acteur_uid
                  )
                  VALUES (
                    $<reunionUid>,
                    $<acteurRef>
                  )
                  ON CONFLICT DO NOTHING
                `,
                {
                  reunionUid: reunion.uid,
                  acteurRef,
                },
              )
            }
          }
          const organe = demandeurs.organe
          if (organe !== undefined) {
            if (
              !existingOrganesDemandeursKeys.delete(`${reunion.uid}_${organe.organeRef}`)
            ) {
              await db.none(
                `
                  INSERT INTO reunions_organes_demandeurs (
                    reunion_uid,
                    organe_uid
                  )
                  VALUES (
                    $<reunionUid>,
                    $<organeUid>
                  )
                  ON CONFLICT DO NOTHING
                `,
                {
                  reunionUid: reunion.uid,
                  organeUid: organe.organeRef,
                },
              )
            }
          }
        }

        const organeReuniRef = reunion.organeReuniRef
        if (organeReuniRef !== undefined) {
          if (!existingOrganesReunisKeys.delete(`${reunion.uid}_${organeReuniRef}`)) {
            try {
              await db.none(
                `
                  INSERT INTO reunions_organes_reunis (
                    reunion_uid,
                    organe_uid
                  )
                  VALUES (
                    $<reunionUid>,
                    $<organeReuniRef>
                  )
                  ON CONFLICT DO NOTHING
                `,
                {
                  reunionUid: reunion.uid,
                  organeReuniRef,
                },
              )
            } catch (e) {
              if (e.constraint === "reunions_organes_reunis_organe_uid_fkey") {
                // Key (organe_uid)=(POXXXXXX) is not present in table "organes".
                if (options.verbose) {
                  console.log(`  ${e.detail}`)
                }
              } else {
                throw e
              }
            }
          }
        }

        const participants = reunion.participants
        if (participants !== undefined) {
          for (const { acteurRef } of participants.participantsInternes || []) {
            if (!existingActeursParticipantsKeys.delete(`${reunion.uid}_${acteurRef}`)) {
              try {
                await db.none(
                  `
                    INSERT INTO reunions_acteurs_participants (
                      reunion_uid,
                      acteur_uid
                    )
                    VALUES (
                      $<reunionUid>,
                      $<acteurRef>
                    )
                    ON CONFLICT DO NOTHING
                  `,
                  {
                    reunionUid: reunion.uid,
                    acteurRef,
                  },
                )
              } catch (e) {
                if (e.constraint === "reunions_acteurs_participants_acteur_uid_fkey") {
                  // Key (acteur_uid)=(PAXXXXXX) is not present in table "acteurs".
                  if (options.verbose) {
                    console.log(`  ${e.detail}`)
                  }
                } else {
                  throw e
                }
              }
            }
          }
        }
      }

      // Delete obsolete reunions.
      for (const key of existingActeursDemandeursKeys) {
        const [reunionUid, acteurUid] = key.split("_")
        await db.none(
          `
            DELETE FROM reunions_acteurs_demandeurs
            WHERE
              reunion_uid = $<reunionUid>
              AND acteur_uid = $<acteurUid>
          `,
          {
            acteurUid,
            reunionUid,
          },
        )
      }
      for (const key of existingActeursParticipantsKeys) {
        const [reunionUid, acteurUid] = key.split("_")
        await db.none(
          `
            DELETE FROM reunions_acteurs_participants
            WHERE
              reunion_uid = $<reunionUid>
              AND acteur_uid = $<acteurUid>
          `,
          {
            acteurUid,
            reunionUid,
          },
        )
      }
      for (const key of existingOrganesDemandeursKeys) {
        const [reunionUid, organeUid] = key.split("_")
        await db.none(
          `
            DELETE FROM reunions_organes_demandeurs
            WHERE
              reunion_uid = $<reunionUid>
              AND organe_uid = $<organeUid>
          `,
          {
            organeUid,
            reunionUid,
          },
        )
      }
      for (const key of existingOrganesReunisKeys) {
        const [reunionUid, organeUid] = key.split("_")
        await db.none(
          `
            DELETE FROM reunions_organes_reunis
            WHERE
              reunion_uid = $<reunionUid>
              AND organe_uid = $<organeUid>
          `,
          {
            organeUid,
            reunionUid,
          },
        )
      }
      for (const obsoleteReunionUid of existingReunionsUids) {
        await db.none(
          `
            DELETE FROM reunions
            WHERE
              uid = $<obsoleteReunionUid>
          `,
          {
            obsoleteReunionUid,
          },
        )
      }
    }
  }

  if (enabledDatasets & EnabledDatasets.Amendements) {
    {
      if (!options.silent) {
        console.log('Updating "amendements"…')
      }

      const existingAmendementsUids = new Set(
        (await db.any(
          `
          SELECT uid
          FROM amendements
        `,
        )).map(({ uid }) => uid),
      )

      for (const amendement of Object.values(amendementByUid)) {
        await db.none(
          `
            INSERT INTO amendements (
              uid,
              data,
              texte_loi_uid,
              tri
            )
            VALUES (
              $<uid>,
              $<data:json>,
              $<texteLoiUid>,
              $<tri>
            )
            ON CONFLICT (uid)
            DO UPDATE SET
              data = $<data:json>,
              texte_loi_uid = $<texteLoiUid>,
              tri = $<tri>
          `,
          {
            data: amendement,
            texteLoiUid: amendement.identifiant.saisine.texteLegislatifRef,
            tri: amendement.triAmendement,
            uid: amendement.uid,
          },
        )
        existingAmendementsUids.delete(amendement.uid)
      }

      // Delete obsolete amendements.
      for (const obsoleteAmendementUid of existingAmendementsUids) {
        await db.none(
          `
            DELETE FROM amendements
            WHERE
              uid = $<obsoleteAmendementUid>
          `,
          {
            obsoleteAmendementUid,
          },
        )
      }
    }
  }

  if (enabledDatasets & EnabledDatasets.DossiersLegislatifs) {
    {
      if (!options.silent) {
        console.log('Updating "documents"…')
      }

      const existingDocumentsUids = new Set(
        (await db.any(
          `
          SELECT uid
          FROM documents
        `,
        )).map(({ uid }) => uid),
      )

      for (const document of Object.values(documentByUid)) {
        await db.none(
          `
            INSERT INTO documents (
              uid,
              data
            )
            VALUES (
              $<uid>,
              $<data:json>
            )
            ON CONFLICT (uid)
            DO UPDATE SET
              data = $<data:json>
          `,
          {
            data: document,
            uid: document.uid,
          },
        )
        existingDocumentsUids.delete(document.uid)
      }

      // Delete obsolete documents.
      for (const obsoleteDocumentUid of existingDocumentsUids) {
        await db.none(
          `
            DELETE FROM documents
            WHERE
              uid = $<obsoleteDocumentUid>
          `,
          {
            obsoleteDocumentUid,
          },
        )
      }
    }

    {
      if (!options.silent) {
        console.log('Updating "dossiers"…')
      }

      const existingDossiersUids = new Set(
        (await db.any(
          `
            SELECT uid
            FROM dossiers
          `,
        )).map(({ uid }) => uid),
      )
      const existingDossiersAutocompletions = new Set(
        (await db.any(
          `
            SELECT uid, autocomplete
            FROM dossiers_autocomplete
          `,
        )).map(({ uid, autocomplete }) => `${uid}_${autocomplete}`),
      )
      const existingDossiersTextsSearches = new Set(
        (await db.any(
          `
            SELECT uid, text_search
            FROM dossiers_text_search
          `,
        )).map(({ uid, text_search }) => `${uid}_${text_search}`),
      )

      for (const dossier of Object.values(dossierParlementaireByUid)) {
        const [dateDernierActe, dateProchainActe] = datesProchesFromActes(
          dossier.actesLegislatifs,
          now,
        )
        await db.none(
          `
            INSERT INTO dossiers (
              uid,
              data,
              date_dernier_acte,
              date_prochain_acte,
              legislature
            )
            VALUES (
              $<uid>,
              $<data:json>,
              $<dateDernierActe>,
              $<dateProchainActe>,
              $<legislature>
            )
            ON CONFLICT (uid)
            DO UPDATE SET
              data = $<data:json>,
              date_dernier_acte = $<dateDernierActe>,
              date_prochain_acte = $<dateProchainActe>,
              legislature = $<legislature>
          `,
          {
            data: dossier,
            dateDernierActe,
            dateProchainActe,
            legislature: options.legislature,
            uid: dossier.uid,
          },
        )
        existingDossiersUids.delete(dossier.uid)

        const autocomplete = dossier.titreDossier.titre
        await db.none(
          `
            INSERT INTO dossiers_autocomplete (
              uid,
              autocomplete
            )
            VALUES (
              $<uid>,
              $<autocomplete>
            )
            ON CONFLICT (uid, autocomplete)
            DO NOTHING
          `,
          {
            autocomplete,
            uid: dossier.uid,
          },
        )
        existingDossiersAutocompletions.delete(`${dossier.uid}_${autocomplete}`)

        const textSearch = dossier.titreDossier.titre
        await db.none(
          `
            INSERT INTO dossiers_text_search (
              uid,
              text_search
            )
            VALUES (
              $<uid>,
              setweight(to_tsvector('french', $<textSearch>), 'A')
            )
            ON CONFLICT (uid, text_search)
            DO NOTHING
          `,
          {
            textSearch,
            uid: dossier.uid,
          },
        )
        existingDossiersTextsSearches.delete(`${dossier.uid}_${textSearch}`)
      }

      // Delete obsolete dossiers.
      for (const key of existingDossiersAutocompletions) {
        const [uid, autocomplete] = key.split("_")
        await db.none(
          `
            DELETE FROM dossiers_autocomplete
            WHERE
              uid = $<uid>
              AND autocomplete = $<autocomplete>
          `,
          {
            autocomplete,
            uid,
          },
        )
      }
      for (const key of existingDossiersTextsSearches) {
        const [uid, textSearch] = key.split("_")
        await db.none(
          `
            DELETE FROM dossiers_text_search
            WHERE
              uid = $<uid>
              AND text_search = $<textSearch>
          `,
          {
            textSearch,
            uid,
          },
        )
      }
      for (const obsoleteDossierUid of existingDossiersUids) {
        await db.none(
          `
            DELETE FROM dossiers
            WHERE
              uid = $<obsoleteDossierUid>
          `,
          {
            obsoleteDossierUid,
          },
        )
      }
    }
  }

  if (enabledDatasets & EnabledDatasets.Scrutins) {
    {
      if (!options.silent) {
        console.log('Updating "scrutins"…')
      }

      const existingScrutinsUids = new Set(
        (await db.any(
          `
          SELECT uid
          FROM scrutins
        `,
        )).map(({ uid }) => uid),
      )

      for (const scrutin of Object.values(scrutinByUid)) {
        await db.none(
          `
            INSERT INTO scrutins (
              uid,
              data
            )
            VALUES (
              $<uid>,
              $<data:json>
            )
            ON CONFLICT (uid)
            DO UPDATE SET
              data = $<data:json>
          `,
          {
            data: scrutin,
            uid: scrutin.uid,
          },
        )
        existingScrutinsUids.delete(scrutin.uid)
      }

      // Delete obsolete scrutins.
      for (const obsoleteScrutinUid of existingScrutinsUids) {
        await db.none(
          `
            DELETE FROM scrutins
            WHERE
              uid = $<obsoleteScrutinUid>
          `,
          {
            obsoleteScrutinUid,
          },
        )
      }
    }
  }

  // Restore standard conversion of dates to JSON.
  Date.prototype.toJSON = existingDateToJson
}

main().catch(error => {
  console.log(error)
  process.exit(1)
})
