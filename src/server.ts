import bodyParser from "body-parser"
import cors from "cors"
import express from "express"
// import fs from "fs"
// import { GraphQLServer } from "graphql-yoga"
import http from "http"
import serveStatic from "serve-static"

import { checkDatabase } from "./database"
import * as acteursController from "./routes/acteurs"
import * as amendementsController from "./routes/amendements"
import * as documentsController from "./routes/documents"
import * as dossiersController from "./routes/dossiers"
import * as organesController from "./routes/organes"
import * as reunionsController from "./routes/reunions"
import * as scrutinsController from "./routes/scrutins"
import serverConfig from "./server_config"

const { NODE_ENV } = process.env
const dev = NODE_ENV === "development"

// const acteurDefs = fs.readFileSync("graphql/acteur.gql", { encoding: "utf-8" })
// const organeDefs = fs.readFileSync("graphql/organe.gql", { encoding: "utf-8" })

// const queryDefs = `
//   type Query {
//     acteur(uid: String!): Acteur
//     acteurs: [Acteur!]!
//     organe(uid: String!): Organe
//     organes: [Organe!]!
//   }
// `

// const typeDefs = `
//   scalar Date
//   scalar DateTime
//   ${acteurDefs}
//   ${organeDefs}
//   ${queryDefs}
// `

// const resolvers = {
//   Query: {
//     acteur(_: any, { uid }: { uid: string }): Acteur {
//       return acteurByUid[uid]
//     },
//     acteurs(_: any): Acteur[] {
//       return Object.values(acteurByUid).sort((a, b) =>
//         a.uid.length === b.uid.length
//           ? a.uid.localeCompare(b.uid)
//           : a.uid.length - b.uid.length,
//       )
//     },
//     organe(_: any, { uid }: { uid: string }): Organe {
//       return organeByUid[uid]
//     },
//     organes(_: any): Organe[] {
//       return Object.values(organeByUid).sort((a, b) =>
//         a.uid.length === b.uid.length
//           ? a.uid.localeCompare(b.uid)
//           : a.uid.length - b.uid.length,
//       )
//     },
//   },
// }

// const server = new GraphQLServer({ typeDefs, resolvers })

// server.start(() => console.log("Server is running on localhost:4000"))

const app = express()

// app.set("title", config.title)
app.set("trust proxy", serverConfig.proxy)

// Enable Express case-sensitive and strict options.
app.enable("case sensitive routing")
app.enable("strict routing")

if (dev) {
  // Add logging.
  const morgan = require("morgan")
  app.use(morgan("dev"))
}

app.use(cors())
// Sirv has no @types/sirv package yet => Use serve-static instead.
// app.use(sirv(serverConfig.dataDir, { dev }))
app.use(serveStatic(serverConfig.dataDir))
app.use(bodyParser.json({ limit: "5mb" }))

app.get("/acteurs", acteursController.listActeurs)
app.get("/acteurs/deputes_actifs", acteursController.listDeputesActifs)
app.get(
  "/acteurs/:acteurUid",
  acteursController.requireActeur,
  acteursController.accessActeur,
)

app.get("/amendements", amendementsController.listAmendements)
app.get(
  "/amendements/:amendementUid",
  amendementsController.requireAmendement,
  amendementsController.accessAmendement,
)

app.get("/documents", documentsController.listDocuments)
app.post(
  "/documents/texte_loi",
  documentsController.accessContenuTexteLoi,
)
app.get(
  "/documents/:documentUid",
  documentsController.requireDocument,
  documentsController.accessDocument,
)
app.get(
  "/documents/:documentUid/amendements",
  documentsController.listTexteLoiAmendements,
)

app.get("/dossiers", dossiersController.listDossiers)
app.get(
  "/dossiers/:dossierUid",
  dossiersController.requireDossier,
  dossiersController.accessDossier,
)

app.get("/organes", organesController.listOrganes)
app.get(
  "/organes/:organeUid",
  organesController.requireOrgane,
  organesController.accessOrgane,
)

app.get("/reunions", reunionsController.listReunions)
app.get(
  "/reunions/:reunionUid",
  reunionsController.requireReunion,
  reunionsController.accessReunion,
)

app.get("/scrutins", scrutinsController.listScrutins)
app.get(
  "/scrutins/:scrutinUid",
  scrutinsController.requireScrutin,
  scrutinsController.accessScrutin,
)

function startExpress() {
  const host = serverConfig.listen.host
  const port = serverConfig.listen.port || serverConfig.port
  const server = http.createServer(app)
  server.listen(port, host, () => {
    console.log(`Listening on ${host || "*"}:${port}...`)
    // // Set up the WebSocket for handling GraphQL subscriptions.
    // new SubscriptionServer(
    //   {
    //     execute,
    //     schema: graphqlController.schema,
    //     subscribe,
    //   },
    //   {
    //     server,
    //     path: "/subscriptions",
    //   },
    // )
  })
  // server.timeout = 30 * 60 * 1000 // 30 minutes (in milliseconds)
}

checkDatabase()
  .then(startExpress)
  .catch((error: any) => {
    console.log(error.stack || error)
    process.exit(1)
  })
