import assert from "assert"
import PgPromise from "pg-promise"

import serverConfig from "./server_config"

export const pgPromise = PgPromise()
export const db = pgPromise({
  database: serverConfig.db.database,
  host: serverConfig.db.host,
  password: serverConfig.db.password,
  port: serverConfig.db.port,
  user: serverConfig.db.user,
})
export let dbSharedConnectionObject: PgPromise.IConnected<unknown> | null = null

export const versionNumber = 1

export async function checkDatabase() {
  // Check that database exists.
  dbSharedConnectionObject = await db.connect()

  assert(
    await existsTable("version"),
    'Database is not initialized. Run "npm run configure" to configure it.',
  )

  let version = await db.one("SELECT * FROM version")
  assert(
    version.number <= versionNumber,
    "Database format is too recent. Upgrade Tricoteuses software.",
  )
  assert.strictEqual(
    version.number,
    versionNumber,
    'Database must be upgraded. Run "npm run configure" to configure it.',
  )
}

async function existsTable(tableName: string): Promise<boolean> {
  return (await db.one(
    "SELECT EXISTS(SELECT NULL FROM information_schema.tables WHERE table_name=$1)",
    [tableName],
  )).exists
}

export function extractDataFromEntry(entry: any): any {
  if (entry === null) {
    return null
  }
  return entry.data
}
