import assert from "assert"

import { db, versionNumber } from "./database"

async function configureDatabase() {
  // Check that database exists.
  await db.connect()

  // Table: version
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS version(
        number integer NOT NULL
      )
    `,
  )
  for (const command of [
    "COMMENT ON TABLE version IS 'version of database'",
    "COMMENT ON COLUMN version.number IS 'version number of database schema'",
  ]) {
    await db.none(command)
  }
  let version = await db.oneOrNone("SELECT * FROM version")
  if (version === null) {
    await db.none("INSERT INTO version(number) VALUES ($<number>)", {
      number: versionNumber,
    })
    version = await db.one("SELECT * FROM version")
  }
  assert(
    version.number <= versionNumber,
    `Database is too recent for current version of application: ${version.number} > ${versionNumber}.`,
  )
  if (version.number < versionNumber) {
    console.log(
      `Upgrading database from version ${version.number} to ${versionNumber}…`,
    )
  }

  // Apply patches that must be executed before every table is created.

  if (version.number < 1) {
    await db.none("DROP TABLE amendements")
  }

  // Tables

  // Table: acteurs
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS acteurs (
        uid text NOT NULL PRIMARY KEY,
        data jsonb,
        depute_actif boolean DEFAULT FALSE
      )
    `,
  )
  await db.none(
    "CREATE INDEX IF NOT EXISTS acteurs_depute_actif_idx ON acteurs (depute_actif)",
  )
  for (const command of [
    "COMMENT ON TABLE acteurs IS 'persons (députés, sénateurs, …)'",
    "COMMENT ON COLUMN acteurs.uid IS 'unique ID of acteur (given by Assemblée)'",
    "COMMENT ON COLUMN acteurs.data IS 'JSON data of acteur'",
    "COMMENT ON COLUMN acteurs.depute_actif IS 'true when the acteur is a current député'",
  ]) {
    await db.none(command)
  }

  // Table: amendements
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS amendements (
        uid text NOT NULL PRIMARY KEY,
        data jsonb,
        texte_loi_uid text NOT NULL, -- REFERENCES documents(uid)
        tri text NOT NULL
      )
    `,
  )
  await db.none(
    "CREATE UNIQUE INDEX IF NOT EXISTS amendements_uid_tri_idx ON amendements(uid, tri)",
  )
  for (const command of [
    "COMMENT ON TABLE amendements IS 'amendments of bills'",
    "COMMENT ON COLUMN amendements.uid IS 'unique ID of amendment (given by Assemblée)'",
    "COMMENT ON COLUMN amendements.data IS 'JSON data of amendment'",
    "COMMENT ON COLUMN amendements.texte_loi_uid IS 'unique ID of the bill that this" +
      " amendment modifies (= amendement.identifiant.saisine.texteLegislatifRef)'",
    "COMMENT ON COLUMN amendements.tri IS 'allows to sort amendments by order of" +
      " passage (= amendement.triAmendement)'",
  ]) {
    await db.none(command)
  }

  // Table: documents
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS documents (
        uid text NOT NULL PRIMARY KEY,
        data jsonb
      )
    `,
  )
  for (const command of [
    "COMMENT ON TABLE documents IS 'legislative documents (bills, …)'",
    "COMMENT ON COLUMN documents.uid IS 'unique ID of document (given by Assemblée)'",
    "COMMENT ON COLUMN documents.data IS 'JSON data of document'",
  ]) {
    await db.none(command)
  }

  // Table: dossiers
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS dossiers (
        uid text NOT NULL PRIMARY KEY,
        data jsonb,
        date_dernier_acte timestamptz,
        date_prochain_acte timestamptz,
        legislature int NOT NULL
      )
    `,
  )
  await db.none(
    "CREATE INDEX IF NOT EXISTS dossiers_date_dernier_acte_idx ON dossiers (date_dernier_acte)",
  )
  await db.none(
    "CREATE INDEX IF NOT EXISTS dossiers_date_prochain_acte_idx ON dossiers (date_prochain_acte)",
  )
  await db.none(
    "CREATE INDEX IF NOT EXISTS dossiers_legislature_idx ON dossiers (legislature)",
  )
  for (const command of [
    "COMMENT ON TABLE dossiers IS 'set of documents and operations produced during the" +
      " legislative process of a bill'",
    "COMMENT ON COLUMN dossiers.uid IS 'unique ID of dossier (given by Assemblée)'",
    "COMMENT ON COLUMN dossiers.data IS 'JSON data of dossier'",
    "COMMENT ON COLUMN dossiers.date_dernier_acte IS 'date & time of the latest acte'",
    "COMMENT ON COLUMN dossiers.date_prochain_acte IS 'date & time of the next acte'",
    "COMMENT ON COLUMN dossiers.legislature IS 'number of the legislature'",
  ]) {
    await db.none(command)
  }
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS dossiers_autocomplete (
        uid text NOT NULL REFERENCES dossiers(uid) ON DELETE CASCADE,
        autocomplete text NOT NULL,
        PRIMARY KEY (uid, autocomplete)
      )
    `,
  )
  await db.none(
    `
      CREATE INDEX IF NOT EXISTS dossiers_autocomplete_trigrams_idx
      ON dossiers_autocomplete
      USING GIST (autocomplete gist_trgm_ops)
    `,
  )
  for (const command of [
    "COMMENT ON TABLE dossiers_autocomplete IS 'autocompletions texts for dossiers'",
    "COMMENT ON COLUMN dossiers_autocomplete.uid IS 'unique ID of dossier" +
      " (same as dossiers)'",
    "COMMENT ON COLUMN dossiers_autocomplete.autocomplete IS 'autocompletion text of" +
      " dossier'",
  ]) {
    await db.none(command)
  }
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS dossiers_text_search (
        uid text NOT NULL REFERENCES dossiers(uid) ON DELETE CASCADE,
        text_search tsvector NOT NULL,
        PRIMARY KEY (uid, text_search)
      )
    `,
  )
  await db.none(
    `
      CREATE INDEX IF NOT EXISTS dossiers_text_search_idx
      ON dossiers_text_search
      USING GIN (text_search)
    `,
  )
  for (const command of [
    "COMMENT ON TABLE dossiers_text_search IS 'full text search for dossiers'",
    "COMMENT ON COLUMN dossiers_text_search.uid IS 'unique ID of dossier" +
      " (same as dossiers)'",
    "COMMENT ON COLUMN dossiers_text_search.text_search IS 'full text search vector" +
      " of dossier'",
  ]) {
    await db.none(command)
  }

  // Table: organes
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS organes (
        uid text NOT NULL PRIMARY KEY,
        data jsonb
      )
    `,
  )
  for (const command of [
    "COMMENT ON TABLE organes IS 'organizations (commissions, groupes, …)'",
    "COMMENT ON COLUMN organes.uid IS 'unique ID of organe (given by Assemblée)'",
    "COMMENT ON COLUMN organes.data IS 'JSON data of organe'",
  ]) {
    await db.none(command)
  }

  // Table: reunions
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS reunions (
        uid text NOT NULL PRIMARY KEY,
        data jsonb,
        jour_debut date NOT NULL,
        jour_fin date NOT NULL,
        lieu_code text,
        xsi_type text
      )
    `,
  )
  await db.none(
    "CREATE INDEX IF NOT EXISTS reunions_jour_debut_idx ON reunions (jour_debut)",
  )
  await db.none(
    "CREATE INDEX IF NOT EXISTS reunions_jour_fin_idx ON reunions (jour_fin)",
  )
  await db.none(
    "CREATE INDEX IF NOT EXISTS reunions_lieu_code_idx ON reunions (lieu_code)",
  )
  await db.none(
    "CREATE INDEX IF NOT EXISTS reunions_xsi_type_idx ON reunions (xsi_type)",
  )
  for (const command of [
    "COMMENT ON TABLE reunions IS 'public meetings'",
    "COMMENT ON COLUMN reunions.uid IS 'unique ID of meeting (given by Assemblée)'",
    "COMMENT ON COLUMN reunions.data IS 'JSON data of meeting'",
    "COMMENT ON COLUMN reunions.jour_debut IS 'first day of meeting'",
    "COMMENT ON COLUMN reunions.jour_fin IS 'last day of meeting'",
    "COMMENT ON COLUMN reunions.lieu_code IS 'unique identifier of place where meeting occurs'",
    "COMMENT ON COLUMN reunions.xsi_type IS 'type of meeting'",
  ]) {
    await db.none(command)
  }

  // Table: reunions_acteurs_demandeurs
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS reunions_acteurs_demandeurs (
        reunion_uid text NOT NULL REFERENCES reunions(uid),
        acteur_uid text NOT NULL REFERENCES acteurs(uid),
        PRIMARY KEY (reunion_uid, acteur_uid)
      )
    `,
  )
  for (const command of [
    "COMMENT ON TABLE reunions_acteurs_demandeurs IS 'persons that have requested the public meetings'",
    "COMMENT ON COLUMN reunions_acteurs_demandeurs.reunion_uid IS 'unique ID of meeting'",
    "COMMENT ON COLUMN reunions_acteurs_demandeurs.acteur_uid IS 'unique ID of person'",
  ]) {
    await db.none(command)
  }

  // Table: reunions_acteurs_participants
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS reunions_acteurs_participants (
        reunion_uid text NOT NULL REFERENCES reunions(uid),
        acteur_uid text NOT NULL REFERENCES acteurs(uid),
        PRIMARY KEY (reunion_uid, acteur_uid)
      )
    `,
  )
  for (const command of [
    "COMMENT ON TABLE reunions_acteurs_participants IS 'persons that are participating to the public meetings'",
    "COMMENT ON COLUMN reunions_acteurs_participants.reunion_uid IS 'unique ID of meeting'",
    "COMMENT ON COLUMN reunions_acteurs_participants.acteur_uid IS 'unique ID of person'",
  ]) {
    await db.none(command)
  }

  // Table: reunions_organes_demandeurs
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS reunions_organes_demandeurs (
        reunion_uid text NOT NULL REFERENCES reunions(uid),
        organe_uid text NOT NULL REFERENCES organes(uid),
        PRIMARY KEY (reunion_uid, organe_uid)
      )
    `,
  )
  for (const command of [
    "COMMENT ON TABLE reunions_organes_demandeurs IS 'organizations that are participating to the public meetings'",
    "COMMENT ON COLUMN reunions_organes_demandeurs.reunion_uid IS 'unique ID of meeting'",
    "COMMENT ON COLUMN reunions_organes_demandeurs.organe_uid IS 'unique ID of organization'",
  ]) {
    await db.none(command)
  }

  // Table: reunions_organes_reunis
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS reunions_organes_reunis (
        reunion_uid text NOT NULL REFERENCES reunions(uid),
        organe_uid text NOT NULL REFERENCES organes(uid),
        PRIMARY KEY (reunion_uid, organe_uid)
      )
    `,
  )
  for (const command of [
    "COMMENT ON TABLE reunions_organes_reunis IS 'organizations the have requested the public meetings'",
    "COMMENT ON COLUMN reunions_organes_reunis.reunion_uid IS 'unique ID of meeting'",
    "COMMENT ON COLUMN reunions_organes_reunis.organe_uid IS 'unique ID of organization'",
  ]) {
    await db.none(command)
  }

  // Table: scrutins
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS scrutins (
        uid text NOT NULL PRIMARY KEY,
        data jsonb
      )
    `,
  )
  for (const command of [
    "COMMENT ON TABLE scrutins IS 'ballots of public votings'",
    "COMMENT ON COLUMN scrutins.uid IS 'unique ID of scrutin (given by Assemblée)'",
    "COMMENT ON COLUMN scrutins.data IS 'JSON data of scrutin'",
  ]) {
    await db.none(command)
  }

  // Apply patches that must be executed after every table is created.

  // Upgrade version number if needed.

  const previousVersionNumber = version.number

  version.number = versionNumber
  assert(
    version.number >= previousVersionNumber,
    `Error in database upgrade script: Wrong version number: ${version.number} < ${previousVersionNumber}.`,
  )
  if (version.number !== previousVersionNumber) {
    await db.none("UPDATE version SET number = $1", version.number)
    console.log(
      `Upgraded database from version ${previousVersionNumber} to ${version.number}.`,
    )
  }
}

configureDatabase()
  .then(() => process.exit(0))
  .catch(error => {
    console.log(error.stack || error)
    process.exit(1)
  })
