import { Acteur, getActeurs } from "./acteurs"
import { Amendement, getAmendements } from "./amendements"
import { Division, Document, getDocuments } from "./documents"
import { ActeLegislatif, DossierParlementaire, getDossiers } from "./dossiers"
import { getOrganes, Organe } from "./organes"
import { getReunions, Reunion } from "./reunions"
import { getScrutins, Scrutin } from "./scrutins"

export const allFollows = [
  "acte.organe",
  "acte.provenance",
  "acte.rapporteurs.acteur",
  "acte.reunion",
  "acte.texteAdopte",
  "acte.texteAssocie",
  "acte.textesAssocies.texteAssocie",
  "acte.votes",
  "acteur.mandats.organes",
  "amendement.identifiant.saisine.texteLegislatif",
  "amendement.signataires.auteur.acteur",
  "amendement.signataires.auteur.groupePolitique",
  "amendement.signataires.auteur.organe",
  "amendement.signataires.cosignataires",
  "document.auteurs.acteur.acteur",
  "document.auteurs.organe",
  "document.dossier",
  "dossier.initiateur.acteurs.acteur",
  "dossier.initiateur.organe",
  "organe.organeParent",
  "reunion.demandeurs.acteurs.acteur",
  "reunion.demandeurs.organe.organe",
  "reunion.organeReuni",
  "reunion.participants.participantsInternes.acteur",
]

export class DataGetter {
  acteurByUid: { [uid: string]: Acteur } = {}
  amendementByUid: { [uid: string]: Amendement } = {}
  documentByUid: { [uid: string]: Document } = {}
  dossierByUid: { [uid: string]: DossierParlementaire } = {}
  organeByUid: { [uid: string]: Organe } = {}
  reunionByUid: { [uid: string]: Reunion } = {}
  scrutinByUid: { [uid: string]: Scrutin } = {}
  requestedActeursUids: Set<string> = new Set()
  requestedAmendementsUids: Set<string> = new Set()
  requestedDocumentsUids: Set<string> = new Set()
  requestedDossiersUids: Set<string> = new Set()
  requestedOrganesUids: Set<string> = new Set()
  requestedReunionsUids: Set<string> = new Set()
  requestedScrutinsUids: Set<string> = new Set()
  visitedActeursUids: Set<string> = new Set()
  visitedAmendementsUids: Set<string> = new Set()
  visitedDocumentsUids: Set<string> = new Set()
  visitedDossiersUids: Set<string> = new Set()
  visitedOrganesUids: Set<string> = new Set()
  visitedReunionsUids: Set<string> = new Set()
  visitedScrutinsUids: Set<string> = new Set()

  constructor(public follow: Set<string>) {}

  addActeur(acteur: Acteur): void {
    this.acteurByUid[acteur.uid] = acteur

    if (acteur.mandats !== undefined) {
      for (const mandat of acteur.mandats) {
        if (this.follow.has("acteur.mandats.organes")) {
          for (const organeRef of mandat.organesRefs) {
            this.requestOrgane(organeRef)
          }
        }
      }
    }
  }

  addAmendement(amendement: Amendement): void {
    this.amendementByUid[amendement.uid] = amendement

    const signataires = amendement.signataires
    const auteur = signataires.auteur
    if (this.follow.has("amendement.signataires.auteur.acteur")) {
      const acteurRef = auteur.acteurRef
      if (acteurRef !== undefined) {
        this.requestActeur(acteurRef)
      }
    }
    if (this.follow.has("amendement.signataires.auteur.groupePolitique")) {
      const groupePolitiqueRef = auteur.groupePolitiqueRef
      if (groupePolitiqueRef !== undefined) {
        this.requestOrgane(groupePolitiqueRef)
      }
    }
    if (this.follow.has("amendement.signataires.auteur.organe")) {
      const organeRef = auteur.organeRef
      if (organeRef !== undefined) {
        this.requestOrgane(organeRef)
      }
    }
    if (this.follow.has("amendement.signataires.cosignataires")) {
      const cosignatairesRefs = signataires.cosignatairesRefs
      if (cosignatairesRefs !== undefined) {
        for (const cosignataireRef of cosignatairesRefs) {
          this.requestActeur(cosignataireRef)
        }
      }
    }

    if (this.follow.has("amendement.identifiant.saisine.texteLegislatif")) {
      this.requestDocument(amendement.identifiant.saisine.texteLegislatifRef)
    }
  }

  addDivision(division: Division): void {
    // this.divisionByUid[division.uid] = division

    for (const auteur of division.auteurs) {
      if (this.follow.has("document.auteurs.acteur.acteur")) {
        const acteurSummary = auteur.acteur
        if (acteurSummary !== undefined) {
          this.requestActeur(acteurSummary.acteurRef)
        }
      }

      if (
        this.follow.has("document.auteurs.acteur.acteur") &&
        auteur.organeRef !== undefined
      ) {
        this.requestOrgane(auteur.organeRef)
      }
    }

    if (this.follow.has("document.dossier")) {
      this.requestDossier(division.dossierRef)
    }

    for (const childDivision of division.divisions || []) {
      this.addDivision(childDivision)
    }
  }

  addDocument(document: Document): void {
    this.documentByUid[document.uid] = document

    for (const auteur of document.auteurs) {
      if (this.follow.has("document.auteurs.acteur.acteur")) {
        const acteurSummary = auteur.acteur
        if (acteurSummary !== undefined) {
          this.requestActeur(acteurSummary.acteurRef)
        }
      }

      if (
        this.follow.has("document.auteurs.acteur.acteur") &&
        auteur.organeRef !== undefined
      ) {
        this.requestOrgane(auteur.organeRef)
      }
    }

    if (this.follow.has("document.dossier")) {
      this.requestDossier(document.dossierRef)
    }

    for (const division of document.divisions || []) {
      this.addDivision(division)
    }
  }

  addDossier(dossier: DossierParlementaire): void {
    this.dossierByUid[dossier.uid] = dossier

    for (const acte of dossier.actesLegislatifs) {
      this.followActe(acte)
    }

    const initiateur = dossier.initiateur
    if (initiateur !== undefined) {
      for (const acteurSummary of initiateur.acteurs || []) {
        if (this.follow.has("dossier.initiateur.acteurs.acteur")) {
          this.requestActeur(acteurSummary.acteurRef)
        }
      }
      if (
        this.follow.has("dossier.initiateur.organe") &&
        initiateur.organeRef !== undefined
      ) {
        this.requestOrgane(initiateur.organeRef)
      }
    }
  }

  addOrgane(organe: Organe): void {
    this.organeByUid[organe.uid] = organe

    if (this.follow.has("organe.organeParent") && organe.organeParentRef !== undefined) {
      this.requestOrgane(organe.organeParentRef)
    }

    // TODO
  }

  addReunion(reunion: Reunion): void {
    this.reunionByUid[reunion.uid] = reunion

    const demandeurs = reunion.demandeurs
    if (demandeurs !== undefined) {
      if (this.follow.has("reunion.demandeurs.acteurs.acteur")) {
        for (const acteurSummary of demandeurs.acteurs || []) {
          this.requestActeur(acteurSummary.acteurRef)
        }
      }
      if (this.follow.has("reunion.demandeurs.organe.organe")) {
        const organeSummary = demandeurs.organe
        if (organeSummary !== undefined) {
          this.requestOrgane(organeSummary.organeRef)
        }
      }
    }
    if (this.follow.has("reunion.organeReuni") && reunion.organeReuniRef !== undefined) {
      this.requestOrgane(reunion.organeReuniRef)
    }
    if (this.follow.has("reunion.participants.participantsInternes.acteur")) {
      const participants = reunion.participants
      if (participants !== undefined) {
        for (const participantInterne of participants.participantsInternes || []) {
          this.requestActeur(participantInterne.acteurRef)
        }
      }
    }
  }

  addScrutin(scrutin: Scrutin): void {
    this.scrutinByUid[scrutin.uid] = scrutin

    // TODO
  }

  followActe(acte: ActeLegislatif): void {
    if (this.follow.has("acte.organe") && acte.organeRef !== undefined) {
      this.requestOrgane(acte.organeRef)
    }
    if (this.follow.has("acte.provenance") && acte.provenanceRef !== undefined) {
      this.requestOrgane(acte.provenanceRef)
    }
    if (this.follow.has("acte.rapporteurs.acteur")) {
      for (const rapporteur of acte.rapporteurs || []) {
        this.requestActeur(rapporteur.acteurRef)
      }
    }
    if (this.follow.has("acte.reunion") && acte.reunionRef !== undefined) {
      this.requestReunion(acte.reunionRef)
    }
    if (this.follow.has("acte.texteAdopte") && acte.texteAdopteRef !== undefined) {
      this.requestDocument(acte.texteAdopteRef)
    }
    if (this.follow.has("acte.texteAssocie") && acte.texteAssocieRef !== undefined) {
      this.requestDocument(acte.texteAssocieRef)
    }
    if (this.follow.has("acte.textesAssocies.texteAssocie")) {
      for (const textAssocie of acte.textesAssocies || []) {
        this.requestDocument(textAssocie.texteAssocieRef)
      }
    }
    if (this.follow.has("acte.votes")) {
      for (const voteRef of acte.voteRefs || []) {
        this.requestScrutin(voteRef)
      }
    }

    for (const childActe of acte.actesLegislatifs || []) {
      this.followActe(childActe)
    }
  }

  async getAll(): Promise<void> {
    let empty = false
    while (!empty) {
      empty = true
      if (this.requestedActeursUids.size > 0) {
        empty = false
        const requestedActeursUids = [...this.requestedActeursUids]
        this.requestedActeursUids.clear()
        const acteurs = await getActeurs(requestedActeursUids)
        for (const acteur of acteurs) {
          this.addActeur(acteur)
        }
        this.visitedActeursUids = new Set([
          ...this.visitedActeursUids,
          ...requestedActeursUids,
        ])
      }
      if (this.requestedAmendementsUids.size > 0) {
        empty = false
        const requestedAmendementsUids = [...this.requestedAmendementsUids]
        this.requestedAmendementsUids.clear()
        const amendements = await getAmendements(requestedAmendementsUids)
        for (const amendement of amendements) {
          this.addAmendement(amendement)
        }
        this.visitedAmendementsUids = new Set([
          ...this.visitedAmendementsUids,
          ...requestedAmendementsUids,
        ])
      }
      if (this.requestedDocumentsUids.size > 0) {
        empty = false
        const requestedDocumentsUids = [...this.requestedDocumentsUids]
        this.requestedDocumentsUids.clear()
        const documents = await getDocuments(requestedDocumentsUids)
        for (const document of documents) {
          this.addDocument(document)
        }
        this.visitedDocumentsUids = new Set([
          ...this.visitedDocumentsUids,
          ...requestedDocumentsUids,
        ])
      }
      if (this.requestedDossiersUids.size > 0) {
        empty = false
        const requestedDossiersUids = [...this.requestedDossiersUids]
        this.requestedDossiersUids.clear()
        const dossiers = await getDossiers(requestedDossiersUids)
        for (const dossier of dossiers) {
          this.addDossier(dossier)
        }
        this.visitedDossiersUids = new Set([
          ...this.visitedDossiersUids,
          ...requestedDossiersUids,
        ])
      }
      if (this.requestedOrganesUids.size > 0) {
        empty = false
        const requestedOrganesUids = [...this.requestedOrganesUids]
        this.requestedOrganesUids.clear()
        const organes = await getOrganes(requestedOrganesUids)
        for (const organe of organes) {
          this.addOrgane(organe)
        }
        this.visitedOrganesUids = new Set([
          ...this.visitedOrganesUids,
          ...requestedOrganesUids,
        ])
      }
      if (this.requestedReunionsUids.size > 0) {
        empty = false
        const requestedReunionsUids = [...this.requestedReunionsUids]
        this.requestedReunionsUids.clear()
        const reunions = await getReunions(requestedReunionsUids)
        for (const reunion of reunions) {
          this.addReunion(reunion)
        }
        this.visitedReunionsUids = new Set([
          ...this.visitedReunionsUids,
          ...requestedReunionsUids,
        ])
      }
      if (this.requestedScrutinsUids.size > 0) {
        empty = false
        const requestedScrutinsUids = [...this.requestedScrutinsUids]
        this.requestedScrutinsUids.clear()
        const scrutins = await getScrutins(requestedScrutinsUids)
        for (const scrutin of scrutins) {
          this.addScrutin(scrutin)
        }
        this.visitedScrutinsUids = new Set([
          ...this.visitedScrutinsUids,
          ...requestedScrutinsUids,
        ])
      }
    }
  }

  requestActeur(acteurUid: string): void {
    if (!this.visitedActeursUids.has(acteurUid)) {
      this.requestedActeursUids.add(acteurUid)
    }
  }

  requestAmendement(amendementUid: string): void {
    if (!this.visitedAmendementsUids.has(amendementUid)) {
      this.requestedAmendementsUids.add(amendementUid)
    }
  }

  requestDocument(documentUid: string): void {
    if (!this.visitedDocumentsUids.has(documentUid)) {
      this.requestedDocumentsUids.add(documentUid)
    }
  }

  requestDossier(dossierUid: string): void {
    if (!this.visitedDossiersUids.has(dossierUid)) {
      this.requestedDossiersUids.add(dossierUid)
    }
  }

  requestOrgane(organeUid: string): void {
    if (!this.visitedOrganesUids.has(organeUid)) {
      this.requestedOrganesUids.add(organeUid)
    }
  }

  requestReunion(reunionUid: string): void {
    if (!this.visitedReunionsUids.has(reunionUid)) {
      this.requestedReunionsUids.add(reunionUid)
    }
  }

  requestScrutin(scrutinUid: string): void {
    if (!this.visitedScrutinsUids.has(scrutinUid)) {
      this.requestedScrutinsUids.add(scrutinUid)
    }
  }

  toJson(): any {
    const json: any = {}
    if (Object.keys(this.acteurByUid).length > 0) {
      json.acteurByUid = this.acteurByUid
    }
    if (Object.keys(this.amendementByUid).length > 0) {
      json.amendementByUid = this.amendementByUid
    }
    if (Object.keys(this.documentByUid).length > 0) {
      json.documentByUid = this.documentByUid
    }
    if (Object.keys(this.dossierByUid).length > 0) {
      json.dossierByUid = this.dossierByUid
    }
    if (Object.keys(this.organeByUid).length > 0) {
      json.organeByUid = this.organeByUid
    }
    if (Object.keys(this.reunionByUid).length > 0) {
      json.reunionByUid = this.reunionByUid
    }
    if (Object.keys(this.scrutinByUid).length > 0) {
      json.scrutinByUid = this.scrutinByUid
    }
    return json
  }
}
