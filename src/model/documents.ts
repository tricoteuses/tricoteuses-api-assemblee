import { Division, Document } from "@tricoteuses/assemblee"
export { retrieveTexteLoiParsed } from "@tricoteuses/assemblee/lib/parsers"

import { db, extractDataFromEntry } from "../database"

export { Division, Document }

export const getDocument = async (uid: string): Promise<Document> => {
  return extractDataFromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM documents
        where uid = $<uid>
      `,
      {
        uid,
      },
    ),
  )
}

export const getDocuments = async (uids: string[]): Promise<Document[]> => {
  if (uids.length === 0) {
    return []
  }
  return (await db.any(
    `
      SELECT *
      FROM documents
      WHERE uid IN ($<uids:list>)
    `,
    {
      uids,
    },
  )).map(extractDataFromEntry)
}
