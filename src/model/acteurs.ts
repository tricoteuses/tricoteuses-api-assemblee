import { Acteur } from "@tricoteuses/assemblee"

import { db, extractDataFromEntry } from "../database"

export { Acteur }

export const getActeur = async (uid: string): Promise<Acteur> => {
  return extractDataFromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM acteurs
        where uid = $<uid>
      `,
      {
        uid,
      },
    ),
  )
}

export const getActeurs = async (uids: string[]): Promise<Acteur[]> => {
  if (uids.length === 0) {
    return []
  }
  return (await db.any(
    `
      SELECT *
      FROM acteurs
      WHERE uid IN ($<uids:list>)
    `,
    {
      uids,
    },
  )).map(extractDataFromEntry)
}
