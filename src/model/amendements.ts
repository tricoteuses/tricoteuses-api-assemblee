import { Amendement } from "@tricoteuses/assemblee"

import { db, extractDataFromEntry } from "../database"

export { Amendement }

export const getAmendement = async (uid: string): Promise<Amendement> => {
  return extractDataFromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM amendements
        where uid = $<uid>
      `,
      {
        uid,
      },
    ),
  )
}

export const getAmendements = async (uids: string[]): Promise<Amendement[]> => {
  if (uids.length === 0) {
    return []
  }
  return (await db.any(
    `
      SELECT *
      FROM amendements
      WHERE uid IN ($<uids:list>)
    `,
    {
      uids,
    },
  )).map(extractDataFromEntry)
}
