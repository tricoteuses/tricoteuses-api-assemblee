import { Organe } from "@tricoteuses/assemblee"

import { db, extractDataFromEntry } from "../database"

export { Organe }

export const getOrgane = async (uid: string): Promise<Organe> => {
  return extractDataFromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM organes
        where uid = $<uid>
      `,
      {
        uid,
      },
    ),
  )
}

export const getOrganes = async (uids: string[]): Promise<Organe[]> => {
  if (uids.length === 0) {
    return []
  }
  return (await db.any(
    `
      SELECT *
      FROM organes
      WHERE uid IN ($<uids:list>)
    `,
    {
      uids,
    },
  )).map(extractDataFromEntry)
}
