import { Scrutin } from "@tricoteuses/assemblee"

import { db, extractDataFromEntry } from "../database"

export { Scrutin }

export const getScrutin = async (uid: string): Promise<Scrutin> => {
  return extractDataFromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM scrutins
        where uid = $<uid>
      `,
      {
        uid,
      },
    ),
  )
}

export const getScrutins = async (uids: string[]): Promise<Scrutin[]> => {
  if (uids.length === 0) {
    return []
  }
  return (await db.any(
    `
      SELECT *
      FROM scrutins
      WHERE uid IN ($<uids:list>)
    `,
    {
      uids,
    },
  )).map(extractDataFromEntry)
}
