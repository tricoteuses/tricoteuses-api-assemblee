import { Reunion } from "@tricoteuses/assemblee"

import { db, extractDataFromEntry } from "../database"

export { Reunion }

export const getReunion = async (uid: string): Promise<Reunion> => {
  return extractDataFromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM reunions
        where uid = $<uid>
      `,
      {
        uid,
      },
    ),
  )
}

export const getReunions = async (uids: string[]): Promise<Reunion[]> => {
  if (uids.length === 0) {
    return []
  }
  return (await db.any(
    `
      SELECT *
      FROM reunions
      WHERE uid IN ($<uids:list>)
    `,
    {
      uids,
    },
  )).map(extractDataFromEntry)
}
