import { ActeLegislatif, DossierParlementaire } from "@tricoteuses/assemblee"

import { db, extractDataFromEntry } from "../database"

export { ActeLegislatif, DossierParlementaire }

export const getDossier = async (uid: string): Promise<DossierParlementaire> => {
  return extractDataFromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM dossiers
        where uid = $<uid>
      `,
      {
        uid,
      },
    ),
  )
}

export const getDossiers = async (uids: string[]): Promise<DossierParlementaire[]> => {
  if (uids.length === 0) {
    return []
  }
  return (await db.any(
    `
      SELECT *
      FROM dossiers
      WHERE uid IN ($<uids:list>)
    `,
    {
      uids,
    },
  )).map(extractDataFromEntry)
}
