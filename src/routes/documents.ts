import { validateNonEmptyTrimmedString } from "@biryani/core"
import fetch from "cross-fetch"
import { NextFunction, Request, Response } from "express"

import { db, extractDataFromEntry } from "../database"
import { wrapAsyncMiddleware } from "../middlewares"
import {
  allFollows,
  DataGetter,
  Document,
  getDocument,
  retrieveTexteLoiParsed,
} from "../model"
import { validateFollow } from "../validators/query"

interface DocumentRequest extends Request {
  document: Document
}

export const accessContenuTexteLoi = wrapAsyncMiddleware(
  async function accessContenuTexteLoi(req: Request, res: Response) {
    const [body, error] = validateAccessContenuTexteLoiBody(req.body)
    if (error !== null) {
      console.error(
        `Error in form:\n${JSON.stringify(body, null, 2)}\n\nError:\n${JSON.stringify(
          error,
          null,
          2,
        )}`,
      )
      res.writeHead(400, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            ...body,
            error: {
              code: 400,
              details: error,
              message: "Invalid body for form",
            },
          },
          null,
          2,
        ),
      )
    }

    const assembleeUrl = new URL(
      body.url,
      "http://www.assemblee-nationale.fr/",
    ).toString()
    const texteLoiParsed = await retrieveTexteLoiParsed(fetch, assembleeUrl)

    const { error: parseError, page } = texteLoiParsed
    if (parseError) {
      console.error(
        `Error while getting page "${assembleeUrl}":\n\nError:\n${JSON.stringify(
          parseError,
          null,
          2,
        )}`,
      )
      res.writeHead(400, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            error: {
              code: 400,
              details: parseError,
              message: `Retrieval of HTML page ${assembleeUrl} failed`,
              page,
            },
          },
          null,
          2,
        ),
      )
    }

    res.writeHead(200, {
      "Content-Type": "application/json; charset=utf-8",
    })
    res.end(JSON.stringify(texteLoiParsed, null, 2))
  },
)

export const accessDocument = wrapAsyncMiddleware(async function accessDocument(
  req: DocumentRequest,
  res: Response,
) {
  const [query, error] = validateAccessDocumentQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const { document } = req

  const dataGetter = new DataGetter(follow)
  dataGetter.addDocument(document)
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({ ...dataGetter.toJson(), uid: document.uid }, null, 2))
})

export const listDocuments = wrapAsyncMiddleware(async function listDocuments(
  req: Request,
  res: Response,
) {
  const [query, error] = validateListDocumentsQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const documents = (await db.any(
    `
      SELECT *
      FROM documents
      LIMIT 10
    `,
  )).map(extractDataFromEntry)

  const dataGetter = new DataGetter(follow)
  for (const document of documents) {
    dataGetter.addDocument(document)
  }
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        ...dataGetter.toJson(),
        uids: documents.map(({ uid }) => uid),
      },
      null,
      2,
    ),
  )
})

export const listTexteLoiAmendements = wrapAsyncMiddleware(
  async function listTexteLoiAmendements(req: Request, res: Response) {
    const { documentUid } = req.params

    const [query, error] = validateListTexteLoiAmendementsQuery(req.query)
    if (error !== null) {
      console.error(
        `Error in ${req.path}:\n${JSON.stringify(
          query,
          null,
          2,
        )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
      )
      res.writeHead(400, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            ...query,
            error: {
              code: 400,
              details: error,
              message: "Invalid query",
            },
          },
          null,
          2,
        ),
      )
    }
    const { follow } = query

    const amendements = (await db.any(
      `
        SELECT *
        FROM amendements
        WHERE
          texte_loi_uid = $<documentUid>
        ORDER BY tri
      `,
      {
        documentUid,
      },
    )).map(extractDataFromEntry)

    const dataGetter = new DataGetter(follow)
    for (const amendement of amendements) {
      dataGetter.addAmendement(amendement)
    }
    await dataGetter.getAll()

    res.writeHead(200, {
      "Content-Type": "application/json; charset=utf-8",
    })
    res.end(
      JSON.stringify(
        {
          ...dataGetter.toJson(),
          uids: amendements.map(({ uid }) => uid),
        },
        null,
        2,
      ),
    )
  },
)

export const requireDocument = wrapAsyncMiddleware(async function requireDocument(
  req: DocumentRequest,
  res: Response,
  next: NextFunction,
) {
  const { documentUid } = req.params

  const document = await getDocument(documentUid)
  if (document === null) {
    res.writeHead(404, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `No document with uid "${documentUid}".`,
          },
        },
        null,
        2,
      ),
    )
  }
  req.document = document

  return next()
})

function validateAccessDocumentQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateAccessContenuTexteLoiBody(body: any): [any, any] {
  if (body === null || body === undefined) {
    return [body, "Missing body"]
  }
  if (typeof body !== "object") {
    return [body, `Expected an object, got ${typeof body}`]
  }

  body = {
    ...body,
  }
  const remainingKeys = new Set(Object.keys(body))
  const errors: { [key: string]: any } = {}

  {
    const key = "url"
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(body[key])
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [body, Object.keys(errors).length === 0 ? null : errors]
}

function validateListDocumentsQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateListTexteLoiAmendementsQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
