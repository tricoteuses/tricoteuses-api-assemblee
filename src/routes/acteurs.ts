import { NextFunction, Request, Response } from "express"

import { db, extractDataFromEntry } from "../database"
import { wrapAsyncMiddleware } from "../middlewares"
import { Acteur, allFollows, DataGetter, getActeur } from "../model"
import { validateFollow } from "../validators/query"

interface ActeurRequest extends Request {
  acteur: Acteur
}

export const accessActeur = wrapAsyncMiddleware(async function accessActeur(
  req: ActeurRequest,
  res: Response,
) {
  const [query, error] = validateAccessActeurQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const { acteur } = req

  const dataGetter = new DataGetter(follow)
  dataGetter.addActeur(acteur)
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({ ...dataGetter.toJson(), uid: acteur.uid }, null, 2))
})

export const listActeurs = wrapAsyncMiddleware(async function listActeurs(
  req: Request,
  res: Response,
) {
  const [query, error] = validateListActeursQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const acteurs = (await db.any(
    `
      SELECT *
      FROM acteurs
      LIMIT 10
    `,
  )).map(extractDataFromEntry)

  const dataGetter = new DataGetter(follow)
  for (const acteur of acteurs) {
    dataGetter.addActeur(acteur)
  }
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        ...dataGetter.toJson(),
        uids: acteurs.map(({ uid }) => uid),
      },
      null,
      2,
    ),
  )
})

export const listDeputesActifs = wrapAsyncMiddleware(async function listDeputesActifs(
  req: Request,
  res: Response,
) {
  const [query, error] = validateListActeursQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const acteurs = (await db.any(
    `
      SELECT *
      FROM acteurs
      WHERE depute_actif
      LIMIT 10
    `,
  )).map(extractDataFromEntry)

  const dataGetter = new DataGetter(follow)
  for (const acteur of acteurs) {
    dataGetter.addActeur(acteur)
  }
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        ...dataGetter.toJson(),
        uids: acteurs.map(({ uid }) => uid),
      },
      null,
      2,
    ),
  )
})

export const requireActeur = wrapAsyncMiddleware(async function requireActeur(
  req: ActeurRequest,
  res: Response,
  next: NextFunction,
) {
  const { acteurUid } = req.params

  const acteur = await getActeur(acteurUid)
  if (acteur === null) {
    res.writeHead(404, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `No acteur with uid "${acteurUid}".`,
          },
        },
        null,
        2,
      ),
    )
  }
  req.acteur = acteur

  return next()
})

function validateAccessActeurQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateListActeursQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
