import { NextFunction, Request, Response } from "express"

import { db, extractDataFromEntry } from "../database"
import { wrapAsyncMiddleware } from "../middlewares"
import { Organe, allFollows, DataGetter, getOrgane } from "../model"
import { validateFollow } from "../validators/query"

interface OrganeRequest extends Request {
  organe: Organe
}

export const accessOrgane = wrapAsyncMiddleware(async function accessOrgane(
  req: OrganeRequest,
  res: Response,
) {
  const [query, error] = validateAccessOrganeQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const { organe } = req

  const dataGetter = new DataGetter(follow)
  dataGetter.addOrgane(organe)
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({ ...dataGetter.toJson(), uid: organe.uid }, null, 2))
})

export const listOrganes = wrapAsyncMiddleware(async function listOrganes(
  req: Request,
  res: Response,
) {
  const [query, error] = validateListOrganesQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const organes = (await db.any(
    `
      SELECT *
      FROM organes
      LIMIT 10
    `,
  )).map(extractDataFromEntry)

  const dataGetter = new DataGetter(follow)
  for (const organe of organes) {
    dataGetter.addOrgane(organe)
  }
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        ...dataGetter.toJson(),
        uids: organes.map(({ uid }) => uid),
      },
      null,
      2,
    ),
  )
})

export const requireOrgane = wrapAsyncMiddleware(async function requireOrgane(
  req: OrganeRequest,
  res: Response,
  next: NextFunction,
) {
  const { organeUid } = req.params

  const organe = await getOrgane(organeUid)
  if (organe === null) {
    res.writeHead(404, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `No organe with uid "${organeUid}".`,
          },
        },
        null,
        2,
      ),
    )
  }
  req.organe = organe

  return next()
})

function validateAccessOrganeQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateListOrganesQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
