import { NextFunction, Request, Response } from "express"

import { db, extractDataFromEntry } from "../database"
import { wrapAsyncMiddleware } from "../middlewares"
import { Scrutin, allFollows, DataGetter, getScrutin } from "../model"
import { validateFollow } from "../validators/query"

interface ScrutinRequest extends Request {
  scrutin: Scrutin
}

export const accessScrutin = wrapAsyncMiddleware(async function accessScrutin(
  req: ScrutinRequest,
  res: Response,
) {
  const [query, error] = validateAccessScrutinQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const { scrutin } = req

  const dataGetter = new DataGetter(follow)
  dataGetter.addScrutin(scrutin)
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({ ...dataGetter.toJson(), uid: scrutin.uid }, null, 2))
})

export const listScrutins = wrapAsyncMiddleware(async function listScrutins(
  req: Request,
  res: Response,
) {
  const [query, error] = validateListScrutinsQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const scrutins = (await db.any(
    `
      SELECT *
      FROM scrutins
      LIMIT 10
    `,
  )).map(extractDataFromEntry)

  const dataGetter = new DataGetter(follow)
  for (const scrutin of scrutins) {
    dataGetter.addScrutin(scrutin)
  }
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        ...dataGetter.toJson(),
        uids: scrutins.map(({ uid }) => uid),
      },
      null,
      2,
    ),
  )
})

export const requireScrutin = wrapAsyncMiddleware(async function requireScrutin(
  req: ScrutinRequest,
  res: Response,
  next: NextFunction,
) {
  const { scrutinUid } = req.params

  const scrutin = await getScrutin(scrutinUid)
  if (scrutin === null) {
    res.writeHead(404, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `No scrutin with uid "${scrutinUid}".`,
          },
        },
        null,
        2,
      ),
    )
  }
  req.scrutin = scrutin

  return next()
})

function validateAccessScrutinQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateListScrutinsQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
