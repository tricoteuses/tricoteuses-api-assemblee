import { validateDateIso8601String, validateMissing, validateOption } from "@biryani/core"
import { NextFunction, Request, Response } from "express"

import { db, extractDataFromEntry } from "../database"
import { wrapAsyncMiddleware } from "../middlewares"
import { allFollows, DataGetter, getReunion, Reunion } from "../model"
import { validateActeurUid, validateOrganeUid } from "../validators/assemblee"
import { validateFollow } from "../validators/query"

interface ReunionRequest extends Request {
  reunion: Reunion
}

export const accessReunion = wrapAsyncMiddleware(async function accessReunion(
  req: ReunionRequest,
  res: Response,
) {
  const [query, error] = validateAccessReunionQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const { reunion } = req

  const dataGetter = new DataGetter(follow)
  dataGetter.addReunion(reunion)
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({ ...dataGetter.toJson(), uid: reunion.uid }, null, 2))
})

export const listReunions = wrapAsyncMiddleware(async function listReunions(
  req: Request,
  res: Response,
) {
  const [query, error] = validateListReunionsQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { acteur: acteurUid, debut, fin, follow, organe: organeUid } = query

  const whereActeurClauses = []
  if (acteurUid !== null) {
    whereActeurClauses.push(
      `uid IN (
        SELECT reunion_uid
        FROM reunions_acteurs_demandeurs
        WHERE acteur_uid = $<acteurUid>
      )`,
      `uid IN (
        SELECT reunion_uid
        FROM reunions_acteurs_participants
        WHERE acteur_uid = $<acteurUid>
      )`,
      "xsi_type = 'seance_type' AND lieu_code = 'AN'",
    )
  }
  const whereActeurClause =
    whereActeurClauses.length === 0 ? "" : `(${whereActeurClauses.join(" OR ")})`

  const whereOrganeClauses = []
  if (organeUid !== null) {
    whereOrganeClauses.push(
      `uid IN (
        SELECT reunion_uid
        FROM reunions_organes_demandeurs
        WHERE organe_uid = $<organeUid>
      )`,
      `uid IN (
        SELECT reunion_uid
        FROM reunions_organes_reunis
        WHERE organe_uid = $<organeUid>
      )`,
    )
  }
  const whereOrganeClause =
    whereOrganeClauses.length === 0 ? "" : `(${whereOrganeClauses.join(" OR ")})`

  const whereClauses = [
    whereActeurClause,
    whereOrganeClause,
    "jour_debut <= $<fin>",
    "jour_fin >= $<debut>",
  ].filter(clause => clause)
  const whereClause =
    whereClauses.length === 0 ? "" : "WHERE " + whereClauses.join(" AND ")

  const reunions = (await db.any(
    `
      SELECT *
      FROM reunions
      ${whereClause}
    `,
    {
      acteurUid,
      debut,
      fin,
      organeUid,
    },
  )).map(extractDataFromEntry)

  const dataGetter = new DataGetter(follow)
  for (const reunion of reunions) {
    dataGetter.addReunion(reunion)
  }
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        ...dataGetter.toJson(),
        uids: reunions.map(({ uid }) => uid),
      },
      null,
      2,
    ),
  )
})

export const requireReunion = wrapAsyncMiddleware(async function requireReunion(
  req: ReunionRequest,
  res: Response,
  next: NextFunction,
) {
  const { reunionUid } = req.params

  const reunion = await getReunion(reunionUid)
  if (reunion === null) {
    res.writeHead(404, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `No reunion with uid "${reunionUid}".`,
          },
        },
        null,
        2,
      ),
    )
  }
  req.reunion = reunion

  return next()
})

function validateAccessReunionQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateListReunionsQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "acteur"
    remainingKeys.delete(key)
    const [value, error] = validateOption(validateMissing, validateActeurUid)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of ["debut", "fin"]) {
    remainingKeys.delete(key)
    const [value, error] = validateDateIso8601String(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "organe"
    remainingKeys.delete(key)
    const [value, error] = validateOption(validateMissing, validateOrganeUid)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  if (
    errors.acteur === undefined &&
    data.acteur === null &&
    errors.organe === undefined &&
    data.organe === null
  ) {
    errors.acteur = errors.organe = 'Either "acteur" or "organe" is required'
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
