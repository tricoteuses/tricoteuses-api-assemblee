import { NextFunction, Request, Response } from "express"

import { db, extractDataFromEntry } from "../database"
import { wrapAsyncMiddleware } from "../middlewares"
import { allFollows, Amendement, DataGetter, getAmendement } from "../model"
import { validateFollow } from "../validators/query"

interface AmendementRequest extends Request {
  amendement: Amendement
}

export const accessAmendement = wrapAsyncMiddleware(async function accessAmendement(
  req: AmendementRequest,
  res: Response,
) {
  const [query, error] = validateAccessAmendementQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const { amendement } = req

  const dataGetter = new DataGetter(follow)
  dataGetter.addAmendement(amendement)
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({ ...dataGetter.toJson(), uid: amendement.uid }, null, 2))
})

export const listAmendements = wrapAsyncMiddleware(async function listAmendements(
  req: Request,
  res: Response,
) {
  const [query, error] = validateListAmendementsQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const amendements = (await db.any(
    `
        SELECT *
        FROM amendements
        LIMIT 10
      `,
  )).map(extractDataFromEntry)

  const dataGetter = new DataGetter(follow)
  for (const amendement of amendements) {
    dataGetter.addAmendement(amendement)
  }
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        ...dataGetter.toJson(),
        uids: amendements.map(({ uid }) => uid),
      },
      null,
      2,
    ),
  )
})

export const requireAmendement = wrapAsyncMiddleware(async function requireAmendement(
  req: AmendementRequest,
  res: Response,
  next: NextFunction,
) {
  const { amendementUid } = req.params

  const amendement = await getAmendement(amendementUid)
  if (amendement === null) {
    res.writeHead(404, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `No amendement with uid "${amendementUid}".`,
          },
        },
        null,
        2,
      ),
    )
  }
  req.amendement = amendement

  return next()
})

function validateAccessAmendementQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateListAmendementsQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
