import querystring from "querystring"

import {
  validateDateIso8601String,
  validateInteger,
  validateMaybeTrimmedString,
  validateMissing,
  validateOption,
  validateSetValue,
  validateStringToNumber,
  validateTest,
} from "@biryani/core"
import { NextFunction, Request, Response } from "express"

import { db, extractDataFromEntry } from "../database"
import { wrapAsyncMiddleware } from "../middlewares"
import { allFollows, DataGetter, DossierParlementaire, getDossier } from "../model"
import { validateFollow } from "../validators/query"

interface DossierRequest extends Request {
  dossier: DossierParlementaire
}

export const accessDossier = wrapAsyncMiddleware(async function accessDossier(
  req: DossierRequest,
  res: Response,
) {
  const [query, error] = validateAccessDossierQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { follow } = query

  const { dossier } = req

  const dataGetter = new DataGetter(follow)
  dataGetter.addDossier(dossier)
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({ ...dataGetter.toJson(), uid: dossier.uid }, null, 2))
})

export const listDossiers = wrapAsyncMiddleware(async function listDossiers(
  req: Request,
  res: Response,
) {
  const [query, error] = validateListDossiersQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }
  const { date, follow, legislature, limit, offset, q: term } = query

  const joinClauses: string[] = []
  const whereClauses: string[] = []
  if (date !== null) {
    whereClauses.push("(date_dernier_acte >= $<date> OR date_prochain_acte NOTNULL)")
  }
  if (legislature !== null) {
    whereClauses.push("legislature = $<legislature>")
  }
  if (term !== null) {
    whereClauses.push(`uid IN (
      SELECT uid
      FROM dossiers_text_search
      WHERE text_search @@ plainto_tsquery('french', $<term>)
    )`)
  }
  const joinsClause = joinClauses.length > 0 ? joinClauses.join(" ") : ""
  const whereClause = whereClauses.length > 0 ? "WHERE " + whereClauses.join(" AND ") : ""

  const dossiers = (await db.any(
    `
      SELECT *
      FROM dossiers
      ${joinsClause}
      ${whereClause}
      ORDER BY
        date_prochain_acte DESC NULLS LAST,
        date_dernier_acte DESC NULLS LAST
      OFFSET $<offset>
      LIMIT $<limit>
    `,
    {
      date,
      legislature,
      limit: limit + 1,
      offset,
      term,
    },
  )).map(extractDataFromEntry)

  let next = null
  if (dossiers.length > limit) {
    dossiers.pop()

    const nextQuery = Object.entries({
      ...query,
      follow: Array.from(follow).sort(),
      offset: offset + limit,
    }).reduce((nextQuery: { [key: string]: any }, [key, value]) => {
      if (value !== null && (!Array.isArray(value) || value.length > 0)) {
        nextQuery[key] = value
      }
      return nextQuery
    }, {})
    next = `${req.protocol}://${req.get("host")}${
      req.originalUrl.split("?")[0]
    }?${querystring.stringify(nextQuery)}`
  }

  const dataGetter = new DataGetter(follow)
  for (const dossier of dossiers) {
    dataGetter.addDossier(dossier)
  }
  await dataGetter.getAll()

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        ...dataGetter.toJson(),
        next,
        uids: dossiers.map(({ uid }) => uid),
      },
      null,
      2,
    ),
  )
})

export const requireDossier = wrapAsyncMiddleware(async function requireDossier(
  req: DossierRequest,
  res: Response,
  next: NextFunction,
) {
  const { dossierUid } = req.params

  const dossier = await getDossier(dossierUid)
  if (dossier === null) {
    res.writeHead(404, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `No dossier with uid "${dossierUid}".`,
          },
        },
        null,
        2,
      ),
    )
  }
  req.dossier = dossier

  return next()
})

function validateAccessDossierQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateListDossiersQuery(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "date"
    remainingKeys.delete(key)
    const [value, error] = validateOption(validateMissing, validateDateIso8601String)(
      data[key],
    )
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "follow"
    remainingKeys.delete(key)
    const [value, error] = validateFollow(allFollows)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "legislature"
    remainingKeys.delete(key)
    const [value, error] = validateOption(validateMissing, [
      validateStringToNumber,
      validateInteger,
      validateTest(value => value >= 0, "Value must be greater than or equal to 0"),
    ])(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "limit"
    remainingKeys.delete(key)
    const [value, error] = validateOption(
      [validateMissing, validateSetValue(10)],
      [
        validateStringToNumber,
        validateInteger,
        validateTest(value => value >= 1, "Value must be greater than or equal to 1"),
        validateTest(value => value <= 100, "Value must be less than or equal to 100"),
      ],
    )(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "offset"
    remainingKeys.delete(key)
    const [value, error] = validateOption(
      [validateMissing, validateSetValue(0)],
      [
        validateStringToNumber,
        validateInteger,
        validateTest(value => value >= 0, "Value must be greater than or equal to 0"),
      ],
    )(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "q"
    remainingKeys.delete(key)
    const [value, error] = validateMaybeTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
