# Tricoteuses-API-Assemblee

## _REST API based on open data of French Assemblée nationale_

This project has **migrated** to https://git.en-root.org/tricoteuses/tricoteuses-api-assemblee.

## Installation

```bash
git clone https://framagit.org/tricoteuses/tricoteuses-api-assemblee
cd tricoteuses-api-assemblee/
```

Create a `.env` file to set PostgreSQL database informations and other configuration variables (you can use `example.env` as a template). Then

```bash
npm install
```

### Database Creation

#### When using _Debian GNU/Linux_

As `root` user:
```bash
apt install postgresql postgresql-contrib

su - postgres
createuser -D -P -R -S assemblee
  Enter password for new role: assemblee
  Enter it again: assemblee
createdb -E utf-8 -O assemblee assemblee
psql assemblee
CREATE EXTENSION IF NOT EXISTS pg_trgm;
exit
logout
```

### Configuration

```bash
npm run configure
```

## Usage

### Retrieval & cleaning of open data from Assemblée nationale

_See README file of [Tricoteuses Assemblée](https://framagit.org/tricoteuses/tricoteuses-assemblee) for details._

```bash
npx babel-node --extensions ".ts" -- node_modules/tricoteuses-assemblee/src/scripts/retrieve_open_data.ts ../assemblee-data/
npx babel-node --extensions ".ts" -- node_modules/tricoteuses-assemblee/src/scripts/reorganize_data.ts ../assemblee-data/
npx babel-node --extensions ".ts" -- node_modules/tricoteuses-assemblee/src/scripts/clean_reorganized_data.ts ../assemblee-data/
npx babel-node --extensions ".ts" -- node_modules/tricoteuses-assemblee/src/scripts/retrieve_deputes_photos.ts --fetch ../assemblee-data/
```

### Import of open data files to database

```bash
npx babel-node --extensions ".ts" -- src/scripts/update_db.ts ../assemblee-data/
```

### Launch development web server

```bash
npm run dev ../assemblee-data/
```
